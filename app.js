const express=require('express')
const crypto = require('crypto');
const as = require('activitystrea.ms');
const sharedSecret = "aa5DqTwoFM6ZODsW1avW/9V8xfdfLvhxQOpWCDtvGnY"; // e.g. "+ZaRRMC8+mpnfGaGsBOmkIFt98bttL5YQRq3p2tXgcE="
//const bufSecret = Buffer.alloc(sharedSecret, "base64");
const got=require('got')
const bodyParser = require('body-parser');
//const{GITLAB_TOKEN,TEAMS_WEBHOOK,GITLAB_ENDPOINT,PORT}=require('./config.js');
const {PORT,TEAMS_WEBHOOK,TEAMS_DEPLOYMENTCHANNEL_WEBHOOK}=require('./config.js');
const param=require('./index.js')
const cors=require('cors');
//var str='';
//console.log(str);
//console.log(param['first']())
/*console.log(typeof(param))
console.log(param)
let arr=param.map((item)=>{
    console.log(item)
    if(item.hasOwnProperty('first')){
        return item['first']()
    }
})*/
//console.log(arr.length)
 //console.log(param.first())
//console.log(param.hasOwnProperty('first')?param['first']():'no')
//const [first,second]=param.params();
//console.log(first())
//console.log(second())
const app=express()
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json())
app.use(cors())
app.get('/',function(req,res){
    res.send('hello world')
})


app.post('/addWatcher',async function(req,res){
    console.log(req.headers['authorization']);
    console.log(req.body.text)
    try{
   let str=req.body.text.split(':').pop()
   let pid=str.includes('<at>')?str.match(/[0-9]+/g).join(''):str
    let options={
          method:'post',
          json:{id: pid,url: TEAMS_WEBHOOK,push_events: 1},
          headers:{
            'PRIVATE-TOKEN': GITLAB_TOKEN
          }
    };
    console.log(options,GITLAB_ENDPOINT);
            console.log('excecuting')
            const response = await got(`${GITLAB_ENDPOINT}/${pid}/hooks`,options);
            res.json({
                "type": "message", 
                "text": "watcher added"
            })
        } catch (error) {
            console.log(error.response.body);
            res.json({
              "type": "message",
              "text": error.response.body
            })
        }
   
})

app.post('/deleteWatcher',async function(req,res){
    let str=req.body.text.split(':').pop()
    let pid=str.includes('<at>')?str.match(/[0-9]+/).join(''):str
    let options={
        headers:{
             'PRIVATE-TOKEN': GITLAB_TOKEN
        }
    };
    try{
        const response= await got(`${GITLAB_ENDPOINT}/${pid}/hooks`,options);
        console.log(response.body)
        let arr=JSON.parse(response.body).map((item)=>{
          console.log(`${item['id']}`)
          return item['id']
         }).join(',')
         console.log('result')
     console.log(arr)
        res.json({
            "type":"message",
            "text": arr
        })
    } catch(error){
        console.log(error.message);
        res.json({
            "type": "message",
            "text": error.message
        })
    }
})

app.post('/gitlabConnector',async function(req,res){
    try{
    console.log(req.body.text)
    let str=req.body.text.match(/:[a-z]+|[0-9]+|:[a-z]+/g).map(item=>{
            return  item.charAt(0)===':'?item.slice(1): item
     })
     console.log('str')
    console.log(str)
    let action=str[0]
    console.log('action')
    console.log(action)
    let payload=str.pop()
    console.log('payload')
    console.log(payload)
    let arr=param.reduce((acc,item)=>{
             if(item.hasOwnProperty(acc)){
                 return item[acc]
             }
             else{
                 return acc
             }
    },action)
    console.log('arr')
    console.log(arr)
   let response=await arr(payload)
   console.log('response')
   console.log(response);
   res.json({
       "type": "message",
       "text": response
   })
    } catch(error){
        console.log(error.message)
        res.json({
            "type": "message",
            "text": error.message
        })
    }

})
app.post('/checkHmac',function(req,res){
     let payload=req.body.text
     var auth = req.headers['authorization'];
    // var msgBuf = Buffer.from(payload, 'utf8');
	 var msgHash = "HMAC " + crypto.createHmac('sha256', bufSecret).update(msgBuf).digest("base64")
     console.log("Computed HMAC: " + msgHash);
     console.log("Received HMAC: " + auth);
     if (msgHash === auth){
         console.log('true')
     }
   
    })

app.post('/gitlabListener',async function(req,res){
    try{
     console.log(req.body)
    let payload=req.body
    console.log(payload.source)
    let teamsPayload={}
    if(payload.event_type=='merge_request'){
       teamsPayload= {
            "@type":"MessageCard",
            "context":"https://schema.org/extensions",
            "summary": "Card \"Test card\"",
            "themeColor": "0078D7",
            "title": `${payload.project.path_with_namespace}`,
            "sections": [
              {
                "activityTitle": `${payload.user.name} pushed in branch <a href=${payload.project.web_url}/commits/${payload.object_attributes.source_branch}>${payload.object_attributes.source_branch}</a>`,
                "activityImage": `${payload.user.avatar_url}`,
                "facts": [
                  {
                    "name": "commitid:",
                    "value": `${payload.object_attributes.last_commit.id}`
                  },
                  {
                    "name": "url:",
                    "value": `<a href=${payload.object_attributes.last_commit.url}>view code</a>`
                  },
                  {
                    "name": "commit message:",
                    "value": `${payload.object_attributes.last_commit.message}`
                  },
                  {
                    "name": "merge status:",
                    "value": `${payload.object_attributes.merge_status}`
                  }
                ]
             }]
          }
    }
    else{
        teamsPayload= {
            "@type":"MessageCard",
            "context":"https://schema.org/extensions",
            "summary": "Card \"Test card\"",
            "themeColor": "0078D7",
            "title": `sy-ux/${payload.repository.name}`,
            "sections": [
              {
                "activityTitle": `${payload.user_name} pushed in branch <a href=${payload.project.web_url}/commits/${payload.project.default_branch}>${payload.project.default_branch}</a>`,
                "activityImage": `${payload.user_avatar}`,
                "facts": payload.commits.map(item=>{
                    return {name:'',value:`<a href=${item.url}>${item.id.substr(0,5)}</a>:${item.message}`}
                })
             }]
          }
    }
    console.log(teamsPayload.sections[0].facts[0].value)
    let options={
        method:'post',
        json: teamsPayload
    }
    let response=await got(TEAMS_WEBHOOK,options)
    res.send();
    }catch(error){
        console.log(error.message)
    }
})
app.post('/login',function(req,res){
    let username=req.body.username;
    let password=req.body.password;
    let data={username:username};
    console.log('login')
    console.log(username)
    console.log(password)
    if(username==='bansi'&&password==='bansi123'){
            res.json({status:'SUCCESS'})
    }
    else{
        res.json({status:'SUCCESS',data:data})
    }
})
app.post('/createPost',function(req,res){
     str=req.body.post
     let data={post:str}
     if(str!=undefined){
         res.json({status:'SUCCESS',data:data})
     }
})
app.post('/onComment',async function(req,res){
    let username=req.body.username
    let comment=req.body.comment
    var result=''
    console.log("name",username)
    const activity = as.create()
    .actor(as.person()
             .name(`${username}`))
    .object(as.note()
              .content(`${comment}`))
    .get();
   console.log('activity')
    console.log(activity)
    await activity.prettyWrite((err, doc) => {
        if (err) throw err;
          result=doc // doc will be a string
      });
      let obj=JSON.parse(result)
      console.log(obj.actor.name)
      let str=`${obj.actor.name} commented "${obj.object.content}' on your post`
      let data={comments:str}
    if(obj!=undefined){
        console.log('true')
        return res.json({status:'SUCCESS',data:data})
    }
})
app.post('/onLike',async function(req,res){
    let username=req.body.username
    let post=req.body.travelPost
    let result=''
    const activity = as.like()
    .actor(as.person()
             .name(`${username}`))
    .object(as.note()
              .content(`${post}`))
    .get();
  // console.log('activity')
   // console.log(activity)
    await activity.prettyWrite((err, doc) => {
        if (err) throw err;
        console.log('res')
        //console.log(doc);
          result=doc // doc will be a string
      });
      let obj=JSON.parse(result)
      console.log(obj.actor.name)
      //console.log(typeof(obj))
     // console.log(obj.hasOwnProperty('type')
      let str=`${obj.actor.name} ${obj.type}s your post "${obj.object.content}"`
      let data={like:str}
      console.log(data.like)
    //console.log(res)
        return res.json({status:'SUCCESS',data:data})
    
     
})

app.post('/deploymentConnector',async function(req,res){
    let result=req.body.text;
    let answers=[];
    answers.push(result);
    console.log(answers)
    let question={
        none:'none',
        serverInfo:'serverInfo',
        requireSSHKey:'requireSSHKey',
        deployTo:'deployTo'
    }
    let lastQuestionAsked=question.none;
    console.log(result)
switch(lastQuestionAsked){
    case question.none:
        lastQuestionAsked=question.serverInfo;
        res.json({
            "type": "message",
            "text": "lets get started.Do you have an existing config file(press yes or no)"
        })
        break;
    case question.serverInfo:
        lastQuestionAsked=question.requireSSHKey;
        res.json({
            "type": "message",
            "text": "please enter server information"
        })
        break;
    case question.requireSSHKey:
        lastQuestionAsked=question.deployTo;
        res.json({
            "type": "message",
            "text": "please enter SSH key"
        })
        break;
    case question.deployTo:
        lastQuestionAsked=question.finish;
        res.json({
            "type": "message",
            "text": "please server where to deploy"
        })
        break;
    case question.finish:
        res.json({
            "type": "message",
            "text": "sit and relax your project will be deployed"
        })
        break;

}
let teamsPayload={
    "@type":"MessageCard",
    "context":"https://schema.org/extensions",
    "summary": "Card \"Test card\"",
    "themeColor": "0078D7",
    "title": `Deployment Approval`,
    "sections": [
      {
        "activityTitle": `please check the deployment info given by the user and approve or reject`,
        "facts": [
            {
              "name": "serverinfo:",
              "value": `myserver.in`
            },
            {
              "name": "deployto:",
              "value": `home/myfolder`
            },
            {
              "name": "description:",
              "value": `tracks all the activities`
            },
          ]
     }],
     "potentialAction": [
        {
          "@type": "ActionCard",
          "name": "No",
          "inputs": [
            {
              "@type": "TextInput",
              "id": "comment",
              "isMultiline": true,
              "title": "Enter your reason"
            }
          ],
          "actions": [
            {
              "@type": "HttpPOST",
              "name": "OK",
              "target": "https://..."
            }
          ]
        },
        {
            "@type": "ActionCard",
            "name": "yes",
            "actions": [
              {
                "@type": "HttpPOST",
                "name": "yes",
                "target": "https://aa8288c5.ngrok.io/approvalStatus",
                "headers":[{
                    'CARD-ACTION-STATUS': "The expense was approved",
                    'CARD-UPDATE-IN-BODY': true 
                }]
                //"body": "projectid"
              }
            ]
          },
        
      ]
    }
    try{
        let options={
            method:'post',
            json: teamsPayload
        }
        let response=await got(TEAMS_DEPLOYMENTCHANNEL_WEBHOOK,options)
        res.send();
        }catch(error){
            console.log(error.message)
        }
    
})

app.post('/approvalStatus',async function(req,res){
    let result=req.body;
    console.log(result)
    /*try{
        let options={
            method:'post',
            json: teamsPayload
        }
        let response=await got(TEAMS_DEPLOYMENTCHANNEL_WEBHOOK,options)
        res.send();
        }catch(error){
            console.log(error.message)
        }*/
        res.send();

})

app.listen(PORT,function(){
    console.log('listening on port 3000')
})